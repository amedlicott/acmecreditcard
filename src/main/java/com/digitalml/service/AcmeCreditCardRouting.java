package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Context.Builder;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.context.JavaBeanValueResolver;
import com.github.jknack.handlebars.context.MapValueResolver;

import static net.logstash.logback.argument.StructuredArguments.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class AcmeCreditCardRouting {

    private static final Logger logger = LoggerFactory.getLogger("acmecreditcard:1.0");

    private static final String cacheURL = "http://infocache-test:9200/cache";
    
    private static final String resourceName = "acmecreditcard";

    public static void main(String[] args) {
   
   		// Initialise index on startup
		Unirest.put(cacheURL).asStringAsync();
   
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
        // Handle timings
        
        Map<Object, Long> timings = new ConcurrentHashMap<>();
        
        before(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.put(request, System.nanoTime());
        	}
        });
        
        after(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		long start = timings.remove(request);
        		long end =  System.nanoTime();
        		logger.info("log message {} {} {} {} ns", value("apiname", "acmecreditcard"), value("apiversion", "1.0"), value("apipath", request.pathInfo()), value("response-timing", (end-start)));
        	}
        });
        
        afterAfter(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.remove(request);
        	}
        });

        get("/acmeCreditCard/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/" + req.params("id") + "/_source").asString();
			
			res.status(cacheResponse.getStatus());
            
            return cacheResponse.getBody();
        });
        
        get("/acmeCreditCard", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "id" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        patch("/acmeCreditCard/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        put("/acmeCreditCard/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/acmeCreditCard", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/acmeCreditCard/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
    }
}